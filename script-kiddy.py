import re
import sys
import time
import random
import argparse
import requests
from pprint import pprint
from bs4 import BeautifulSoup

# Function to validate the domain name
def isValidDomain(domainName):
    pattern = r"(?:[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?\.)+[a-z0-9][a-z0-9-]{0,61}[a-z0-9]"
    return bool(re.match(pattern, domainName))

def getRandomUA(uaList):
    uaString = random.choice(uaList)
    return uaString


# Define your default values
domainName = ''
number = 10
sleep = 0


# Get args if any
description = 'A script kiddy HTTP request generation'
parser = argparse.ArgumentParser(description=description)
parser.add_argument('-d','--domain',help='Domain name you would like to target; type str; not set', type=str, required=True)
parser.add_argument('-n','--number',help='Number of HTTP requests to send; type int; 10 by default', type=int,required=False)
parser.add_argument('-s','--sleep',help='Number of seconds sleep between each request; type int or float; 1 by default', type=(float),required=False)
args = vars(parser.parse_args())
if args['number']:
    number = int(args['number'])
if args['sleep']:
    sleep = int(args['sleep'])
if args['domain']:
    domainName = str(args['domain'])


# validate domain name
if not isValidDomain(domainName):
    message = "Invalid domain name: {}, exiting script".format(domainName)
    sys.exit(message)
startingURL = 'https://{}/'.format(domainName)
print('scraping: {}'.format(startingURL))


# create list of user-agents
uaUrl = 'https://raw.githubusercontent.com/cvandeplas/pystemon/refs/heads/master/user-agents.txt'
r = requests.get(uaUrl)
uaList = (r.text).split("\n")


# Create your request headers
requestHeaders = {
    'Host': domainName,
    'Referer': 'https://{}/'.format(domainName),
    'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:129.0) Gecko/20100101 Firefox/129.0',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/png,image/svg+xml,*/*;q=0.8',
    'Accept-Language': 'en-US,en;q=0.5',
    'Connection': 'keep-alive'
}


# Initialize the list of discovered urls
urls = {}
r = requests.get(startingURL,headers=requestHeaders)
soup = BeautifulSoup(r.text, "html.parser")
link_elements = soup.select("a[href]")
for link_element in link_elements:
    link = link_element['href']
    if startingURL in link:
        urls[link] = 1


# Make random requests
i = 1
while i <= number:
    url = 'https://{}/'.format(domainName)
    if len(urls) > 1:
        url = random.choice(list(urls.keys()))
    uaString = getRandomUA(uaList)
    requestHeaders['User-Agent'] = uaString
    r = requests.get(url,headers=requestHeaders)
    message = 'Attack {: <5}: {: <100} result: {: <10}'.format(i,url,r.status_code)
    print(message)
    time.sleep(sleep)
    i+=1