#  mkdir projectA
#  cd projectA
#  python3 -m venv env
# source env/bin/activate
# pip3 install validators
# pip3 install selenium
# sudo apt install chromium-chromedriver
# deactivate

import json
import argparse
import validators
from time import sleep
from pprint import pprint
from datetime import datetime
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.chrome.options import Options



# instantiate the parser
description = 'Selenium based scraper'
parser = argparse.ArgumentParser(description=description)

# optional argument
parser.add_argument("-l","--location", type=str, help="a string URL or path")

# parse argument
args = parser.parse_args()
location = args.location

# fallback if location not specified
if location:
    print(f"will fetch {location}")
else:
    ymd = datetime.today().strftime('%Y-%m-%d')
    location = f"{ymd}.json"
    print(f"will fetch {location}")

# validate if url
is_url = validators.url(location)

# convert location to json
url_object = {}
if is_url == True:
    'placeholder'
else:
    f = open(location, "r")
    url_object = json.loads(f.read())


# Create a new Chrome WebDriver instance
chrome_driver_path = "/usr/bin/chromedriver" 
service = Service(chrome_driver_path)

options = Options()
options.add_argument("--headless")

driver = webdriver.Chrome(service=service, options=options)

# get urls
validity = {}
for key in url_object:
    url = url_object[key]
    # Navigate to a website
    driver.get(url)
    
    # Get the html page source of the current page
    sleep(1)
    page_source = driver.execute_script("return document.getElementsByTagName('html')[0].innerHTML")
    
    valid = False
    # Check page source for string
    string_check = 'Page not found'
    if string_check not in page_source:
        print(f"{key} link valid")
        valid = True
    else:
        print(f"{key} link NOT valid")

    validity[key] = {'url' : url, 'valid' : valid}

driver.quit()

pprint(validity, indent=4, compact=True, sort_dicts=True, )